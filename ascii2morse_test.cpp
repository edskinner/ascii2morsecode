/* ascii2morse_test.cpp
 * Test driver for ascii2morse.cpp
 *
 * (C) Copyright 2012 by Ed Skinner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Email: ed@flat5.net
 * Web:   http://www.flat5.net/
 *
 */

#include <Arduino.h>
#include "ascii2morse.h"
#include "ascii2morse_send.h"



/* void ascii2morse_everything(void)
 * Send all ASCII characters as morse code
 */

void
ascii2morse_everything()
{
	char	everything[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'-/()\"@=";
	char	* cp;

	/* Walk through the string and send each character */
	for (cp = everything; *cp != '\0'; cp++) {
		ascii2morse_sendChar(*cp);	/* Send one character */
		delay_charSpace();			/* Delay between characters */
	}
}



/* void setup(void)
 * One-time initialization
 */

void
setup()
{
	pinMode(get_ledPin(), OUTPUT);
	set_wpm(15);
	return;
}



/* void loop(void)
 * Do this forever
 */

void
loop()
{
	int	cnt;

	ascii2morse_everything();	/* Send "everything" */
	for (cnt = 1; cnt <= 5; cnt++)	/* Big break when done */
		delay_wordSpace();
	return;
}
