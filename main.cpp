// Built and flashed to Duemilanove board from Eclipse by following the instructions at:
// http://horrorcoding.altervista.org/arduino-development-with-eclipse-a-step-by-step-tutorial-to-the-basic-setup/
// (Minor updates left in the comments section at that web address.)
// 17 April 2012, Ed Skinner

// Split into main.cpp and mystuff.cpp
// 18 April 2012, Ed Skinner

#include <Arduino.h>

extern void setup(void);
extern void loop(void);

int main(void) {
	init();
	setup();

	while(true) {
		loop();
	}
}
