/* ascii2morse_send.h
 * Send morse code
 *
 * (C) Copyright 2012 by Ed Skinner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Email: ed@flat5.net
 * Web:   http://www.flat5.net/
 *
 */

#define DEFAULT_WPM	20	/* Default transmit rate */
#define	DEFAULT_PIN	13	/* Default output LED pin */
#define	LED_ON	HIGH	/* LED On logic level */
#define	LED_OFF	LOW		/* LED Off logic level */

/* Prototypes */

void set_ledPin(int);
int	 get_ledPin(void);
int	 set_wpm(int);
int	 get_wpm(void);
void delay_wordSpace(void);
void delay_charSpace(void);
void send_dot(void);
void send_dash(void);
void send_error(void);
int  ascii2morse_sendChar(int);
