/* ascii2morse.h
 * Blink LED in morse code
 *
 * (C) Copyright 2012 by Ed Skinner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Email: ed@flat5.net
 * Web:   http://www.flat5.net/
 *
 */

/* Prototypes */

int ascii2morse_translate(int c);
int ascii2morse_sendChar(int c);
