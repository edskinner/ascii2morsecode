/* ascii2morse.cpp
 * Convert ASCII to MorseCode and Send It
 *
 * (C) Copyright 2012 by Ed Skinner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Email: ed@flat5.net
 * Web:   http://www.flat5.net/
 *
 */

#include <Arduino.h>
#include "ascii2morse_send.h"

/* a2m_alpha[] - Morse code (leading 1 bit marker) for 'A' through 'Z' */

/* Note: The MorseCode type is predicated on the fact that Morse Code characters
 *       never contain more six elements. Thus, it is possible to represent any
 *       Morse Code character in a standard "char". The Morse Code bits (dots and
 *       dashes) are represented by 0s and 1s in the low-order bits of the char.
 *       Since Morse Code characters are different lengths, a leading 1 is used
 *       to mark the location of the first dot/dash bit which follows immediately.
 *
 *       Decoding is accomplished by counting shifts until 8 bits have been processed.
 *       The high-order bit (2^7) is tested and shifted (counting the shifts) until a
 *       1 is found. The bits thereafter are the dots and dashes (until all 8 bits
 *       have thusly been processed.
 */

static char a2m_alpha[26] = {
	0x05, /* A */
	0x18, /* B */
	0x1a, /* C */
	0x0c, /* D */
	0x02, /* E */
	0x12, /* F */
	0x0e, /* G */
	0x10, /* H */
	0x04, /* I */
	0x17, /* J */
	0x0d, /* K */
	0x14, /* L */
	0x07, /* M */
	0x06, /* N */
	0x0f, /* O */
	0x16, /* P */
	0x1d, /* Q */
	0x0a, /* R */
	0x08, /* S */
	0x03, /* T */
	0x09, /* U */
	0x11, /* V */
	0x0b, /* W */
	0x19, /* X */
	0x1b, /* Y */
	0x1c  /* Z */
};

/* a2m_digit[] - Morse code (leading 1 bit marker) for 'A' through 'Z' */

static char a2m_digit[10] = {
	0x3f, /* 0 */
	0x2f, /* 1 */
	0x27, /* 2 */
	0x23, /* 3 */
	0x21, /* 4 */
	0x20, /* 5 */
	0x30, /* 6 */
	0x38, /* 7 */
	0x3c, /* 8 */
	0xe3, /* 9 */
};

/* int ascii2morse_translate(char c)
 * Translate an ASCII character to morse char-code
 * Input: An 8-bit ASCII character
 * Output: The corresponding MorseCode integer, or
 *         -1 = error.
 *
 * A MorseCode integer has 8 signficant bits with a 1 bit immediately to the left (more
 * significant) than the morse code 1s and 0s that immediately follow.
 * That is, the MorseCode coding for 'A' is 101 and decoded by discarding the left-most 1
 * and then decoding the remaining bits as 0 = dot and 1 = dash. ('A' is dot-dash.)
 * Similarly, the MorseCode coding for 'B' is 11000: skip the first 1, then dash-dot-dot-dot.
 */

int
ascii2morse_translate(int c)
{
	int	answer;

	if ( (c >= 'A') && (c <= 'Z') )
		answer = a2m_alpha[c - 'A'];
	else if ( (c >= 'a') && (c <= 'z') )
		answer = a2m_alpha[c - 'a'];
	else if ( (c >= '0') && (c <= '9') )
		answer = a2m_digit[c - '0'];
	else switch (c) {
		case '.': answer = (0x55); break;
		case ',': answer = (0x73); break;
		case ':': answer = (0x78); break;
		case '?': answer = (0x4c); break;
		case '\'': answer = (0x5e); break;
		case '-': answer = (0x61); break;
		case '/': answer = (0x32); break;
		case '(': answer = (0x6d); break;
		case ')': answer = (0x6d); break;
		case '"': answer = (0x52); break;
		case '@': answer = (0x5a); break;
		case '=': answer = (0x31); break;
		default: answer = (char) -1; break;
	}
	return(answer);
}



/* int ascii2morse_sendChar(int)
 * Returns 0 on success, -1 on failure
 * Send morse code for an ASCII character
 */

int
ascii2morse_sendChar(int c)
{
	int	code, cnt;

	code = ascii2morse_translate(c);
	if (((char) -1) == code) {
		send_error();
		return -1;
	}
	for (cnt = 0; (!(code & 0x80)) && (cnt <= 7); cnt++ )
		code <<= 1;
	for ( ; cnt <= 6; cnt++ ) {
		code <<= 1;
		if (code & 0x80)
			send_dash();
		else
			send_dot();
	}
	return 0;
}

