/* ascii2morse_send.cpp
 * Send morse code
 *
 * (C) Copyright 2012 by Ed Skinner
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Email: ed@flat5.net
 * Web:   http://www.flat5.net/
 *
 */

#include <Arduino.h>
#include "ascii2morse.h"
#include "ascii2morse_send.h"

static	int	ledPin = DEFAULT_PIN;	/* LED output pin */
static	int	wpm = DEFAULT_WPM;		/* Words per minute transmit rate */
static	int	elementTime = 1250 / DEFAULT_WPM;	/* elementTime time (msec) */



/* void set_ledPin(int pin)
 * Set the pin# to be used for the LED
 */

void
set_ledPin(int pin) {
	ledPin = pin;
}



/* int get_ledPin(void)
 * Returns the pin# being used for the LED
 */

int
get_ledPin() {
	return ledPin;
}



/* int set_wpm(int)
 * Set the WPM (words per minute) transmit rate
 * Must be between 5 and 100
 * (Rate is approximate)
 */

int
set_wpm(int req_wpm) {
	if (req_wpm < 5)
		return -1;
	if (req_wpm > 100)
		return -1;
	wpm = req_wpm;
	elementTime = 1250 / wpm;
	return wpm;
}



/* int get_wpm(void)
 * Get the WPM (words per minute) transmit rate
 */

int
get_wpm() {
	return wpm;
}



/* void delay_wordSpace(void)
 * Delay between words (five dot times)
 */

void
delay_wordSpace() {
	int	cnt;

	for (cnt = 1; cnt <= 5; cnt++)
		delay(elementTime);
	return;
}



/* void delay_charSpace(void)
 * Delay between characters (three dot times)
 */

void
delay_charSpace() {
	int	cnt;

	for (cnt = 1; cnt <= 3; cnt++)
		delay(elementTime);
	return;
}



/* void send_dot(void)
 * Send a dot
 */

void
send_dot() {
	digitalWrite(ledPin, LED_ON);
	delay(elementTime);
	digitalWrite(ledPin, LED_OFF);
	delay(elementTime);
	return;
}



/* void send_dash(void)
 * Send a dash
 */

void
send_dash() {
	digitalWrite(ledPin, LED_ON);
	delay(elementTime);
	delay(elementTime);
	delay(elementTime);
	digitalWrite(ledPin, LED_OFF);
	delay(elementTime);
	return;
}



/* void send_error(void)
 * Send an error (eight dots)
 */

void
send_error() {
	int	cnt;

	for (cnt = 0; cnt < 8; cnt++)
		send_dot();
}

